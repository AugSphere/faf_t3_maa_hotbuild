unitkeygroups["T3_Mobile_Anti_Air"] = {
    "dalk003", -- Redeemer
    "delk002", -- Cougar
    "drlk001", -- Bouncer
    "dslk004", -- Uyanah
}

table.removeByValue(unitkeygroups["Arties"],"T3_Mobile_Heavy_Artillery")
table.insert(unitkeygroups["Arties"],"T3_Mobile_Anti_Air")


table.removeByValue(unitkeygroups["TML"],"T3_Mobile_Missile_Platform")
table.removeByValue(unitkeygroups["TML"],"T3_Shield_Disruptor")
table.removeByValue(unitkeygroups["TML"],"T3_Mobile_Shield_Generator")
table.insert(unitkeygroups["TML"],"T3_Mobile_Heavy_Artillery")

if unitkeygroups["T3_Special_Air"] ~= nil then
    m_key = "T3_Special_Air"
else
    m_key = "Upgrades"
end
table.insert(unitkeygroups[m_key],"T3_Mobile_Missile_Platform")
table.insert(unitkeygroups[m_key],"T3_Shield_Disruptor")
table.insert(unitkeygroups[m_key],"T3_Mobile_Shield_Generator")

